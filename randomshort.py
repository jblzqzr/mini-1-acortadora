from webapp import WebApp
import argparse
import shelve
import random
import string

PAGE = """<!DOCTYPE html>
<html lang="en">
<body>
    <form action="" method="post">
        <label for="url">URL to shorten: </label>
        <input type="text" id="url" name="url"><br><br>
        <input type="submit" value="Post">
    </form>
    """

PORT = 1234

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="es">
  <body>
    <h1>Resource not available: {resource}.</h1>
    <p>No URL shortened with this id</p>
  </body>
</html>
"""

PAGE_ERROR = """
<!DOCTYPE html>
<html lang="es">
  <body>
    <h1>Error in the form</h1>
  </body>
</html>
"""


def parse_args():
    parser = argparse.ArgumentParser(description="Simple HTTP Server")
    parser.add_argument('-p', '--port', type=int, default=PORT,
                        help="TCP port for the server")
    arg = parser.parse_args()
    return arg


class RandomShort(WebApp):
    shorts = shelve.open("shortened_url")
    urls = shelve.open("urls")

    def parse(self, request):
        try:
            parsed_request = request.split()[1]
            parsed_method = request.split()[0]
            content = request.split('\r\n\r\n')[1]
        except IndexError:
            parsed_request = None
            parsed_method = None
            content = None
        return parsed_request, parsed_method, content

    def process(self, parsed_request, parsed_method, content):
        if parsed_method == 'GET':
            return self.do_get(parsed_request)
        if parsed_method == 'POST':
            return self.do_post(content)

    def do_get(self, parsed_request):
        resource = parsed_request[1:]  # Nos quedamos con el recurso
        print(resource)
        if parsed_request == '/':
            html = PAGE
            html += "<h1>URLs acortadas</h1>"
            html += "<table border='1'><tr><th>URL</th><th>Acortada</th></tr>"
            for clave, valor in self.shorts.items():
                html += f"<tr><td><a href={clave} target='_blank'>{clave}</a></td><td>{valor}</td></tr>"
            html += "</table></body></html>"
            print("Process: Returning 200 OK")
            return "200 OK", html
        if resource in self.urls:
            location = self.urls[resource]
            response = f"301 Moved Permanently\r\nLocation: {location}\r\n\r\n"
            return response, ""
        else:
            return "404 Not Found", PAGE_NOT_FOUND.format(resource=parsed_request)

    def do_post(self, content):
        qs = content.split('=')
        if qs[0] == 'url':
            if 'http%3A%2F%2F' in qs[1] or 'https%3A%2F%2F' in qs[1]:  # : y / reservados, los codifica.
                url = qs[1]
                url = url.replace('%3A%2F%2F', '://')
            else:
                url = 'https://' + qs[1]
            if url not in self.shorts:
                shortened = ''.join(random.choices(string.ascii_letters + string.digits, k=6))
                self.shorts[url] = shortened
                self.urls[shortened] = url
            html = "<html><head><title>Accortadora URLs</title></head><body>"
            html += "<h1>URLs acortadas</h1>"
            html += "<table border='1'><tr><th>URL</th><th>Acortada</th></tr>"
            for clave, valor in self.shorts.items():
                html += f"<tr><td><a href={clave} target='_blank'>{clave}</a></td><td>{valor}</td></tr>"
            html += "</table></body></html>"
            return "200 OK", html
        else:
            return "400 Bad Request", PAGE_ERROR


if __name__ == "__main__":
    args = parse_args()
    app = RandomShort('localhost', args.port)
