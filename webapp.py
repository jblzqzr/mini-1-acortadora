#!usr/bin/python3

import socket


class WebApp:

    def parse(self, request):
        print("Parse: Parsing request")
        return None, None, None

    def process(self, parsed_request, parsed_method, content):
        print("Process: Returning 200 OK")
        return "200 OK", "<html><body><h1>It works!</h1></body></html>"

    def __init__(self, hostname, port):
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((hostname, port))

        # Queue a maximum of 5 TCP connection requests
        mySocket.listen(5)

        # Accept connections, read incoming data, and call
        # parse and process methods (in a loop)

        while True:
            print("Waiting for connections")
            (recvSocket, address) = mySocket.accept()
            print("HTTP request received (going to parse and process):")
            request = recvSocket.recv(2048)
            parsed_request, parsed_method, content = self.parse(request.decode('utf8'))
            (returnCode, htmlAnswer) = self.process(parsed_request, parsed_method, content)
            print("Answering back...")
            response = "HTTP/1.1 " + returnCode + " \r\n\r\n" \
                       + htmlAnswer + "\r\n"
            recvSocket.send(response.encode('utf8'))
            recvSocket.close()


if __name__ == "__main__":
    testWebApp = WebApp("localhost", 1234)
